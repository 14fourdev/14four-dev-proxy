[TOC]

# Requirements

- [docker](https://www.docker.com/get-started)
- [docker-compose](https://docs.docker.com/compose/)
- Environment Variable `DEV_DOMAIN` (automatically setup with `./setup`)
- Docker `proxy` network (automatically setup with `./setup`)
- Available Ports
    - 80
    - 443
    - 8080

# Setup

1. `./setup`

1. Reload your terminal

    For ZSH `. ~/.zshrc`

    For Bash `. ~/.bash_profile` or `. ~/.bashrc`

1. Start the Traefik containers `docker-compose up -d`

1. Visit you the dashboard `{DEV_DOMAIN}:8080`


# Config

## Docker Compose

You can configure your project to use your new Traefik proxy by configuring the network and labels.

### Network

Make sure to add your service to the `proxy` network; otherwise Treafik won't be able to proxy to your container.

```yaml
services:
  app:
    # ...
    networks:
      - local
      - proxy

networks:
  local:
    external: false
  proxy:
    external: true
```

### Domain

You can configure your docker-compose projects with labels. Add the appropriate labels for your project; Traefik will watch docker and automatically add your container to the proxy.

#### Single Domain



```yaml
services:
  app:
    # ...
    labels:
      - traefik.port=8080
      - traefik.frontend.rule=Host:pepsination-poxa.${DEV_DOMAIN}
      - traefik.docker.network=proxy
```

#### Multiple domains

You can add mutliple domains to the same service.

**Note:** there needs to be a unique namespace for the each domain `traefik.{namespace}.frontend.rule=Host:example.com`

```yaml
services:
  app:
    # ...
    labels:
      - traefik.port=80
      - traefik.direct.frontend.rule=Host:backend.pepsination.${DEV_DOMAIN}
      - traefik.admin.frontend.rule=Host:pepsination.${DEV_DOMAIN}
      - traefik.docker.network=proxy
```

#### Path Prefixing

You Can also setup routing for the domain to mutliple services. You specify a `PathPrefix` for a service to be used on specific routes.

```yaml
services:
  app:
    # ...
    labels:
      - traefik.port=80
      - traefik.frontend.rule=Host:pepsination.${DEV_DOMAIN};PathPrefix:/admin,/vendor/,/storage,/api,/uploads,/svg
      - traefik.docker.network=proxy

  frontend:
    # ...
    labels:
      - traefik.port=3000
      - traefik.proxy.frontend.rule=Host:pepsination.${DEV_DOMAIN}
      - traefik.docker.network=proxy
```

## Custom Configs

If you have need for a custom configuration (i.e. a vagrant box that you want to be accessible on port 80 to others on the network).

You can create custom configuration files and place them in the `/configs` directory. This directory is mapped to the Traefik container and it watches for

#### Example

File: `/configs/my-dreamvention.toml`

```toml
# rules
[backends]
  [backends.mydreamvention]
    [backends.mydreamvention.servers.server1]
      url = "http://192.168.10.26:80"
      weight = 1

[frontends]
  [frontends.frontend1]
    backend = "mydreamvention"
    passHostHeader = true
    [frontends.frontend1.routes.route0]
      rule = "Host:dreamvention2-voting.albertdev.dev.14four.com"
```
