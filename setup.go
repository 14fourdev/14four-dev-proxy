package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"regexp"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
	survey "gopkg.in/AlecAivazis/survey.v1"
)

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

// writeLines writes the lines to the given file.
func writeLines(lineNumber int, lineOutput string, path string) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(file), "\n")

	if len(lines) <= lineNumber {
		lines = append(lines, lineOutput)
	} else {
		lines[lineNumber] = lineOutput
	}

	output := strings.Join(lines, "\n")

	err = ioutil.WriteFile(path, []byte(output), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	devDoamin := os.Getenv("DEV_DOMAIN")

	if devDoamin == "" {
		setEnvironment()
	} else {
		confirm := false
		prompt := &survey.Confirm{
			Message: "Your Dev Domain is set to " + devDoamin + " would you like to change it?",
		}
		survey.AskOne(prompt, &confirm, nil)
		if confirm {
			setEnvironment()
		}
	}

	setupEnv()
	setupAcme()
	setupProxy()

	fmt.Println("Run: docker-compose up -d")
}

func setupEnv() {
	_, err := os.Stat(".env")
	if os.IsNotExist(err) {
		keyPrompt := &survey.Input{
			Message: "What is your GoDaddy API Key (check 1Password - 'Dev Proxy')",
		}
		apiKey := ""
		survey.AskOne(keyPrompt, &apiKey, nil)

		secretPrompt := &survey.Input{
			Message: "What is your GoDaddy API Secret (check 1Password - 'Dev Proxy')",
		}
		apiSecret := ""
		survey.AskOne(secretPrompt, &apiSecret, nil)

		env := fmt.Sprintf(`GODADDY_API_KEY=%s
GODADDY_API_SECRET=%s`, apiKey, apiSecret)
		ioutil.WriteFile(".env", []byte(env), 0644)
	}
}

func setupAcme() {
	_, err := os.Stat("acme.json")
	if os.IsNotExist(err) {
		ioutil.WriteFile("acme.json", []byte(""), 0600)
	}
}

func setupProxy() {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion("1.39"))
	if err != nil {
		log.Fatal(err)
	}

	networks, err := cli.NetworkList(ctx, types.NetworkListOptions{})
	if err != nil {
		log.Fatal(err)
	}

	exists := false
	for _, network := range networks {
		matched, _ := regexp.MatchString("proxy", network.Name)
		if matched {
			exists = true
		}
	}

	if !exists {
		cli.NetworkCreate(ctx, "proxy", types.NetworkCreate{Driver: "bridge"})
	}
}

func setEnvironment() {
	domain := ""
	prompt := &survey.Input{
		Message: "What is your dev domain (ex albertdev.14four.com)",
	}
	survey.AskOne(prompt, &domain, nil)

	usr, err := user.Current()
	files, err := ioutil.ReadDir(usr.HomeDir)
	if err != nil {
		log.Fatal(err)
	}
	var profiles = []string{}
	matcher := regexp.MustCompile(`^.(bashrc|bash_profile|zshrc)$`)
	for _, f := range files {
		matched := matcher.MatchString(f.Name())
		if matched {
			profiles = append(profiles, f.Name())
		}
	}

	shellFiles := []string{}
	shells := &survey.MultiSelect{
		Message: "Which Shell do you use:",
		Options: profiles,
		Default: []string{profiles[len(profiles)-1]},
	}
	survey.AskOne(shells, &shellFiles, nil)

	for _, filename := range shellFiles {
		setExportInFile(filename, domain)
	}

	fmt.Printf("Run: source ~/%s\n", shellFiles[len(shellFiles)-1])
}

func setExportInFile(filename string, domain string) {
	usr, err := user.Current()
	path := usr.HomeDir + "/" + filename

	lines, err := readLines(path)
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}

	alreadyExists := 0
	matcher := regexp.MustCompile(`export DEV_DOMAIN=`)
	for i, line := range lines {
		matched := matcher.MatchString(line)
		if matched {
			alreadyExists = i
		}
	}

	lineString := "export DEV_DOMAIN=" + domain

	if alreadyExists > 0 {
		writeLines(alreadyExists, lineString, path)
	} else {
		writeLines(len(lines)+1, lineString, path)
	}
}
